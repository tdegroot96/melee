## How to prepare your ide
- Run command `./gradlew idea` or `./gradlew eclipse`

## How to build desktop module on intellij
- Go to `File` -> `Project Structure` -> `Artifacts` 
- Click on `+` -> `JAR` -> `Empty`
- Set artifact name / jar name to your preferences
- First, create/reuse a `MANIFEST.MF`
- Open `desktop` in `Available Elements` and select all the jars listed there
- Then you `Right click` -> `Extract Into Output Root`
- `Right click` 'desktop' compile output -> `Put in Output Root`
- `Right click` 'core' compile output -> `Put in Output Root`

## How to build html module on intellij
- Run command in terminal: `./gradlew html:dist
    - copy/paste `html/build/dist/*` into your web root
    - Check it out, probably you'll encounter something like: `Couldn't find Type for class ...`
    - The fix is:
    - Add reflection classes/packages to `core/src/*.gwt.xml`
    - Run the dist command again, this is trial and error
- Keep in mind this takes ages and every reflection inclusion make the build process slower, so choose wisely!
- Read more ([...](https://github.com/libgdx/libgdx/wiki/Reflection))