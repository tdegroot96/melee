package nl.tdegroot.games.melee.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import nl.tdegroot.games.melee.Game;

public class DesktopLauncher {
	public static void main (String[] args) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 1280;
        config.height = 720;
        config.resizable = false;
        config.vSyncEnabled = false;
        config.title = Game.TITLE + " v" + Game.VERSION;
        new LwjglApplication(new Game(args), config);
	}
}
