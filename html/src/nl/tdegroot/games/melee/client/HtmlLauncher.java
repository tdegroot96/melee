package nl.tdegroot.games.melee.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.google.gwt.user.client.Window;
import nl.tdegroot.games.melee.Game;

public class HtmlLauncher extends GwtApplication {

    public GwtApplicationConfiguration getConfig() {
        return new GwtApplicationConfiguration(900, 600);
    }

    public ApplicationListener createApplicationListener() {
        boolean debug = Boolean.parseBoolean(Window.Location.getParameter("debug"));
        String[] args = new String[1];
        if (debug) {
            args[0] = "debug";
            Gdx.app.log("Debug", "true");
        } else {
            args[0] = "";
            Gdx.app.log("Debug", "false");
        }
        return new Game(args);
    }

}