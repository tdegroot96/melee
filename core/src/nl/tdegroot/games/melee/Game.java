package nl.tdegroot.games.melee;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import nl.tdegroot.games.melee.level.Level;

public class Game extends ApplicationAdapter {

    public static final String TITLE = "MELEE";
    public static final String VERSION = "0.0.7";
	public static final float PPM = 32;
	public static final int V_WIDTH = 400;
	public static final int V_HEIGHT = 208;

	SpriteBatch batch;
	Texture img;

	private Level level;
	private String[] args;
	private FPSLogger fpsLogger = new FPSLogger();

	public Game(String[] args) {
		this.args = args;
	}

	public void create () {
		batch = new SpriteBatch();
		level = new Level(batch);
		for (String arg : args) {
			Gdx.app.log("Arg", arg);
			if (arg.equals("debug")) {
				level.setDebug(true);
			}
		}
	}

	public void render () {
		Color clearColor = level.getClearColor();
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, 1.0f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        level.tick();
		level.render(batch);
		fpsLogger.log();
	}

	public void dispose() {
		batch.dispose();
		level.dispose();
		super.dispose();
	}
}
