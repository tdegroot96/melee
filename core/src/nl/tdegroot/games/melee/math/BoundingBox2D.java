package nl.tdegroot.games.melee.math;

import com.badlogic.gdx.math.Vector2;

public class BoundingBox2D {

    public Vector2 position;
    public Vector2 size;

    public BoundingBox2D(Vector2 position, Vector2 size) {
        this.position = position;
        this.size = size;
    }

}
