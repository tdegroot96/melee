package nl.tdegroot.games.melee.math;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class AABB {

    public static Vector2 collides(BoundingBox2D a, BoundingBox2D b) {
        float xDiff = (a.position.x - b.position.x);
        if (abs((int) xDiff) < a.size.x + b.size.x) {
            float yDiff = (a.position.y - b.position.y);
            if (abs((int) yDiff) < a.size.y + b.size.y) {
                return new Vector2(xDiff, yDiff);
            }
        }
        return null;
    }

    private static int abs(int i) {
        return i < 0 ? i * -1 : i;
    }

}
