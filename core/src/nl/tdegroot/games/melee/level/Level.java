package nl.tdegroot.games.melee.level;

import box2dLight.RayHandler;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.EllipseMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Polyline;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import nl.tdegroot.games.melee.Game;
import nl.tdegroot.games.melee.entity.DisposableEngine;
import nl.tdegroot.games.melee.entity.EntityIdentifier;
import nl.tdegroot.games.melee.entity.component.*;
import nl.tdegroot.games.melee.entity.component.system.InputSystem;
import nl.tdegroot.games.melee.entity.component.system.RenderSystem;
import nl.tdegroot.games.melee.graphics.ParallaxCamera;
import nl.tdegroot.games.melee.util.GameUtils;

import java.util.ArrayList;
import java.util.List;

public class Level implements ContactListener {

	public static final int PLAYER_BIT = 0x01;
	public static final int COLLISION_BOX_BIT = 0x02;
	public static final int COLLISION_POLY_BIT = 0x04;
	public static final int LIGHT_BIT = 0x08;
	public static final int ELEVATOR_BIT = 0x0f;

	private OrthographicCamera camera;
	private ParallaxCamera backgroundCamera;
	private Viewport gamePort;
	private float cameraBottomOffset;


	private TiledMap map;
	private Texture backgroundParallax;
	private TiledMapTileLayer background, foreground;
	private List<TiledMapTileLayer> others = new ArrayList<>();
	private OrthogonalTiledMapRenderer tiledMapRenderer;

	private Color clearColor = Color.CLEAR;

	private World physicsWorld;
	private Box2DDebugRenderer debugRenderer = new Box2DDebugRenderer();
	private RayHandler rayHandler;

	private float levelWidth;
	private float levelHeight;

	private double accumulator;
	private double currentTime;
	private float timestep = 1 / 60f;

	private boolean debug = false;

	private DisposableEngine engine = new DisposableEngine();
	private Entity entity = new Entity();

	private List<SoundRadius> radiusList = new ArrayList<>();

	public Level(SpriteBatch batch) {
		camera = new OrthographicCamera();
		backgroundCamera = new ParallaxCamera(1280, 720);

		gamePort = new FitViewport(Game.V_WIDTH / Game.PPM, Game.V_HEIGHT / Game.PPM, camera);

		backgroundParallax = new Texture("landscape_rand1.png");

		physicsWorld = new World(new Vector2(0, -80), true);

		rayHandler = new RayHandler(physicsWorld);
		rayHandler.setAmbientLight(0.4f);

		camera.position.set(gamePort.getWorldWidth() / 2, gamePort.getWorldWidth() / 2, 0);
		camera.setToOrtho(false);
		camera.zoom = (1 / Game.PPM) / 2;
		cameraBottomOffset = ((camera.viewportHeight / 12) / Game.PPM);
		try {
			if ((map = new TmxMapLoader().load("dev.tmx")) != null) {
				Gdx.app.log("Dev map", "Loaded");
			}
		} catch (Exception e) {
			Gdx.app.error("Dev map", "Not around");
			map = new TmxMapLoader().load("level.tmx");
		}
		tiledMapRenderer = new TearlessMapRenderer(map, 1 / Game.PPM);
		parseCollisions();

		physicsWorld.setContactListener(this);

		loadLights();
		parseElevators();
		loadEnvironmentalObjects();

		loadMapProperties();

		engine.addEntity(entity);
		EntityIdentifier.next();

		engine.addSystem(new InputSystem());
		engine.addSystem(new RenderSystem(batch));

	}

	private void loadMapProperties() {
		MapProperties properties = map.getProperties();
		String backgroundHex = properties.get("backgroundcolor", String.class);

		if (backgroundHex.length() > 1) {
			clearColor = GameUtils.hexToColor(backgroundHex);
		}

		levelWidth = properties.get("width", int.class) * properties.get("tilewidth", int.class);
		levelHeight = properties.get("height", int.class) * properties.get("tileheight", int.class);

		Array<TiledMapTileLayer> tileLayers = map.getLayers().getByType(TiledMapTileLayer.class);
		for (int i = 0; i < tileLayers.size; i++) {
			TiledMapTileLayer tileLayer = tileLayers.get(i);
			switch (tileLayer.getName().toLowerCase()) {
				case "background":
					background = tileLayer;
					break;
				case "foreground":
					foreground = tileLayer;
					break;
				default:
					others.add(tileLayer);
					break;
			}
		}

		MapLayer objectLayer = map.getLayers().get("Objects");
		if (objectLayer == null) {
			Gdx.app.log("ObjectsLayer", "No objects layer found");
			return;
		}
		MapObjects objectList = objectLayer.getObjects();
		int objectCount = objectList.getCount();
		if (objectCount <= 0) {
			Gdx.app.log("ObjectsLayer", "No collisions found");
			return;
		}
		Gdx.app.log("ObjectsLayer", objectCount + " objects found");
		for (MapObject object : objectList) {
			switch (object.getName().toLowerCase()) {
				case "playerspawn":
					MapProperties objectProperties = object.getProperties();
					float x = objectProperties.get("x", 5f, float.class);
					float y = objectProperties.get("y", levelHeight, float.class);

					PhysicsComponent physicsComponent = new PhysicsComponent(physicsWorld);
					physicsComponent.setBox(x, y, new Vector2(22, 62), new Vector2(64, 64), 0.8f, PLAYER_BIT, COLLISION_BOX_BIT | ELEVATOR_BIT);
					physicsComponent.addSensor();
					entity.add(physicsComponent);
					entity.add(new InputComponent());
					entity.add(new ContactComponent());
					entity.add(new DrawingComponent());
					entity.add(new PlayerComponent());

					break;
				default:
					break;
			}
		}
	}

	private void loadLights() {
		Array<EllipseMapObject> objectList = map.getLayers().get("Lights").getObjects().getByType(EllipseMapObject.class);
		Gdx.app.log("ObjectsLayer", objectList.size + " lights found");
		for (MapObject object : objectList) {
			Ellipse ellipse = ((EllipseMapObject) object).getEllipse();

			float radius = ellipse.width;
			float width = ellipse.width / 2;
			float x = ellipse.x + width;
			float y = ellipse.y + width;
			String hex = object.getProperties().get("color", String.class);
			float opacity = Float.parseFloat(object.getProperties().get("opacity", String.class));
			Color color = GameUtils.hexToColor(hex);
			color.a = opacity;

			GameUtils.createLight(rayHandler, x, y, radius, color);
		}
	}

	private void parseCollisions() {
		BodyDef bodyDef;
		PolygonShape shape;
		FixtureDef fixtureDef;
		Body body;

		for (MapObject object : map.getLayers().get("Collision").getObjects().getByType(RectangleMapObject.class)) {
			Rectangle rect = ((RectangleMapObject) object).getRectangle();

			bodyDef = new BodyDef();
			shape = new PolygonShape();
			fixtureDef = new FixtureDef();

			bodyDef.type = BodyDef.BodyType.StaticBody;
			bodyDef.position.set((rect.x + rect.width / 2) / Game.PPM, (rect.y + rect.height / 2) / Game.PPM);

			body = physicsWorld.createBody(bodyDef);

			shape.setAsBox(rect.width / 2 / Game.PPM, rect.height / 2 / Game.PPM);
			fixtureDef = GameUtils.createFixtureBox(0, 0, rect.width / 2, rect.height / 2);
			fixtureDef.shape = shape;
			fixtureDef.friction = 0.8f;
			fixtureDef.filter.categoryBits = COLLISION_BOX_BIT;
			fixtureDef.filter.maskBits = PLAYER_BIT | LIGHT_BIT;
			body.createFixture(fixtureDef).setUserData("Collision box");
		}

		for (MapObject object : map.getLayers().get("Collision").getObjects().getByType(PolylineMapObject.class)) {
			Polyline polyline = ((PolylineMapObject) object).getPolyline();

			bodyDef = new BodyDef();
			ChainShape chain = new ChainShape();
			fixtureDef = new FixtureDef();

			bodyDef.type = BodyDef.BodyType.StaticBody;
			bodyDef.position.set(polyline.getX() / Game.PPM, polyline.getY() / Game.PPM);

			body = physicsWorld.createBody(bodyDef);

			float[] vertices = polyline.getVertices();
			for (int i = 0; i < vertices.length; i++) {
				vertices[i] = vertices[i] / Game.PPM;
			}
			chain.createChain(vertices);

			fixtureDef.shape = chain;
			fixtureDef.friction = 0.8f;
			fixtureDef.filter.categoryBits = COLLISION_POLY_BIT;
			fixtureDef.filter.maskBits = PLAYER_BIT | LIGHT_BIT;
			body.createFixture(fixtureDef).setUserData("Collision edge");
		}
	}

	private void parseElevators() {
		BodyDef bodyDef;
		PolygonShape shape;
		FixtureDef fixtureDef;
		Body body;

		MapLayer objectLayer = map.getLayers().get("Elevators");
		if (objectLayer == null) {
			Gdx.app.log("Elevators", "No elevator layer found");
			return;
		}

		Array<RectangleMapObject> objectList = objectLayer.getObjects().getByType(RectangleMapObject.class);
		int objectCount = objectList.size;
		if (objectCount <= 0) {
			Gdx.app.log("Elevators", "No elevators found");
			return;
		}

		Gdx.app.log("Elevators", objectList.size + " elevators found");

		for (MapObject object : objectList) {
			Rectangle rect = ((RectangleMapObject) object).getRectangle();

			bodyDef = new BodyDef();
			shape = new PolygonShape();
			fixtureDef = new FixtureDef();

			bodyDef.type = BodyDef.BodyType.StaticBody;
			bodyDef.position.set((rect.x + rect.width / 2) / Game.PPM, (rect.y + rect.height / 2) / Game.PPM);

			body = physicsWorld.createBody(bodyDef);

			shape.setAsBox(rect.width / 2 / Game.PPM, rect.height / 2 / Game.PPM);
			fixtureDef = GameUtils.createFixtureBox(0, 0, rect.width / 2, rect.height / 2);
			fixtureDef.shape = shape;
			fixtureDef.filter.categoryBits = ELEVATOR_BIT;
			fixtureDef.isSensor = true;
			body.createFixture(fixtureDef).setUserData("Elevator box");
		}

	}

	/**
	 * Load environmental objects, such as particles and sounds.
	 * This might be interesting in the future:
	 * [Create ellipse in box2d](http://stackoverflow.com/questions/10032756/how-to-create-ellipse-shapes-in-box2d/20558014)
	 */
	private void loadEnvironmentalObjects() {
		MapLayer layer = map.getLayers().get("Sounds");

		if (layer != null) {
			Array<RectangleMapObject> objectList = layer.getObjects().getByType(RectangleMapObject.class);

			Gdx.app.log("Environmental", objectList.size + " sounds found");
			for (MapObject object : objectList) {
				Rectangle rect = ((RectangleMapObject) object).getRectangle();

				BodyDef bodyDef = new BodyDef();
				FixtureDef fixtureDef;

				bodyDef.type = BodyDef.BodyType.StaticBody;
				bodyDef.position.set((rect.x + rect.width / 2) / Game.PPM, (rect.y + rect.height / 2) / Game.PPM);

				Body body = physicsWorld.createBody(bodyDef);

				fixtureDef = GameUtils.createFixtureBox(0, 0, rect.width / 2, rect.height / 2);
				fixtureDef.isSensor = true;

				SoundRadius soundRadius = new SoundRadius(object, body, new Vector2(rect.width / Game.PPM, rect.height / Game.PPM));

				body.createFixture(fixtureDef).setUserData(soundRadius);
			}
		}

	}

	public void tick() {
		physicsWorld.step(timestep, 6, 2);

		engine.getEntitiesFor(Family.all(PhysicsComponent.class, InputComponent.class, DrawingComponent.class, PlayerComponent.class).get()).first();
		Body body = Mapper.physics.get(entity).body;

		camera.position.x = body.getPosition().x;
		camera.position.y = body.getPosition().y + cameraBottomOffset;
		camera.update();

//		for (SoundRadius radius : radiusList) {
//			Vector2 diff = radius.body.getPosition().sub(body.getPosition());
//			float pan = 0; // -(diff.x / (radius.size.x - 0));
//			float volume = 1; // 1 - Math.abs(pan);
//			radius.music.setPan(pan, volume);
//		}

		tiledMapRenderer.setView(camera);
	}

	public void render(SpriteBatch batch) {
		batch.setProjectionMatrix(camera.combined);

		if (backgroundParallax != null) {
			batch.begin();
			batch.setProjectionMatrix(backgroundCamera.projection);

			batch.draw(backgroundParallax, (-camera.viewportWidth / 2) - camera.position.x, (-camera.viewportHeight / 2) - camera.position.y);

			batch.setProjectionMatrix(camera.combined);
			batch.end();
		}

		tiledMapRenderer.getBatch().begin();

		if (background != null)
			tiledMapRenderer.renderTileLayer(background);

		for (TiledMapTileLayer tileLayer : others) {
			tiledMapRenderer.renderTileLayer(tileLayer);
		}
		tiledMapRenderer.getBatch().end();

		engine.update(Gdx.graphics.getDeltaTime());

		if (debug) {
			debugRenderer.render(physicsWorld, camera.combined);
		}

		if (foreground != null) {
			tiledMapRenderer.getBatch().begin();
			tiledMapRenderer.renderTileLayer(foreground);
			tiledMapRenderer.getBatch().end();
		}

		rayHandler.setCombinedMatrix(camera);
		rayHandler.updateAndRender();
	}

	public TiledMap getMap() {
		return map;
	}

	public Color getClearColor() {
		return clearColor;
	}

	public World getPhysicsWorld() {
		return physicsWorld;
	}

	public void setDebug(boolean debug) {
		this.debug = debug;
	}

	public void dispose() {
		engine.dispose();
		map.dispose();
		rayHandler.dispose();
		physicsWorld.dispose();
	}

	public void beginContact(Contact contact) {
		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();
		Object userData = b.getUserData();
		if (userData.toString().matches("body-(box|ellipse)-sensor-[0-9]+$")) {
			if (a == null) {
				return;
			}

			if (a.getUserData().toString().contains("Collision")) {
				long identifier = Long.parseLong(userData.toString().split("body-(box|ellipse)-sensor-")[1]);
				ContactComponent contactComponent = ContactProvider.getContactByIdentifier(identifier);
				contactComponent.contacts++;
			}
		}

		if (userData.toString().matches("body-(box|ellipse)-[0-9]+$")) {
			if (a.getUserData().toString().contains("Elevator")) {
				long identifier = Long.parseLong(userData.toString().split("body-(box|ellipse)-")[1]);
				ContactComponent contactComponent = ContactProvider.getContactByIdentifier(identifier);
				contactComponent.canClimb = true;
			}
		}
		if (a.getUserData() instanceof SoundRadius) {
			if (userData.toString().matches("body-(box|ellipse)-[0-9]+$")) {
				Gdx.app.log("SoundRadius", "Entered");
				SoundRadius soundRadius = (SoundRadius) a.getUserData();
				for (Music music : soundRadius.musicList) {
					if (!music.isPlaying()) {
						music.setLooping(true);
						music.play();
					}
				}
				if (!radiusList.contains(soundRadius)) {
					radiusList.add(soundRadius);
				}
			}
		}
	}

	public void endContact(Contact contact) {
		Fixture a = contact.getFixtureA();
		Fixture b = contact.getFixtureB();
		Object userData = b.getUserData();
		if (userData.toString().matches("body-(box|ellipse)-sensor-[0-9]+$")) {
			if (a == null) {
				return;
			}

			if (a.getUserData().toString().contains("Collision")) {
				long identifier = Long.parseLong(userData.toString().split("body-(box|ellipse)-sensor-")[1]);
				ContactComponent contactComponent = ContactProvider.getContactByIdentifier(identifier);
				contactComponent.contacts--;
			}
		}

		if (userData.toString().matches("body-(box|ellipse)-[0-9]+$")) {
			if (a.getUserData().toString().contains("Elevator")) {
				long identifier = Long.parseLong(userData.toString().split("body-(box|ellipse)-")[1]);
				ContactComponent contactComponent = ContactProvider.getContactByIdentifier(identifier);
				contactComponent.canClimb = false;
			}
		}
		if (a.getUserData() instanceof SoundRadius) {
			if (userData.toString().matches("body-(box|ellipse)-[0-9]+$")) {
				Gdx.app.log("SoundRadius", "End");
				SoundRadius soundRadius = (SoundRadius) a.getUserData();
				for (Music music : soundRadius.musicList) {
					if (music.isPlaying()) {
						music.pause();
					}
				}
				if (radiusList.contains(soundRadius)) {
					radiusList.remove(soundRadius);
				}
			}
		}
	}

	public void preSolve(Contact contact, Manifold oldManifold) {
	}

	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

}
