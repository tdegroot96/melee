package nl.tdegroot.games.melee.level;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.MusicLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.Disposable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SoundRadius implements Disposable {

	protected MapObject object;
	protected Body body;
	protected Vector2 size;
	protected Music music;

	protected List<Music> musicList = new ArrayList<>();

	private float generalVolume = 0.1f;

	public SoundRadius(MapObject object, Body body, Vector2 size) {
		this.object = object;
		this.body = body;
		this.size = size;
		MapProperties properties = object.getProperties();
		Iterator<String> keys = properties.getKeys();
		while (keys.hasNext()) {
			String key = keys.next();
			if (key.startsWith("sound_")) {
				Gdx.app.log("SoundRadius", key);
				int index = Integer.parseInt(key.split("sound_")[1]);
				music = Gdx.audio.newMusic(Gdx.files.internal(properties.get(key, String.class)));
				musicList.add(index, music);
			} else if (key.startsWith("volume_")) {
				int index = Integer.parseInt(key.split("volume_")[1]);
				float volume = Float.parseFloat(properties.get(key, String.class));
				Music music = musicList.get(index);
				music.setVolume(generalVolume * volume);
			}
		}
	}

	public void dispose() {

	}

}
