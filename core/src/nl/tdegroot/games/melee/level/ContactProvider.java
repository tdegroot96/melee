package nl.tdegroot.games.melee.level;

import com.badlogic.gdx.Gdx;
import nl.tdegroot.games.melee.entity.component.ContactComponent;

import java.util.HashMap;
import java.util.List;

public class ContactProvider {

	private static HashMap<Long, ContactComponent> contactMap = new HashMap<Long, ContactComponent>();

	public static void register(long identifier, ContactComponent contactComponent) {
		contactMap.put(identifier, contactComponent);
		Gdx.app.log("Registered new contact", String.valueOf(identifier));
	}

	public static void unregister(long identifier) {
		contactMap.remove(identifier);
	}

	public static ContactComponent getContactByIdentifier(long identifier) {
		return contactMap.get(identifier);
	}

	public static List<ContactComponent> getContacts() {
		return (List<ContactComponent>) contactMap.values();
	}

}
