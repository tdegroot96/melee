package nl.tdegroot.games.melee.util;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;

public class DummyController implements Controller {

	public boolean getButton(int buttonCode) {
		return false;
	}

	public float getAxis(int axisCode) {
		return 0;
	}

	public PovDirection getPov(int povCode) {
		return null;
	}

	public boolean getSliderX(int sliderCode) {
		return false;
	}

	public boolean getSliderY(int sliderCode) {
		return false;
	}

	public Vector3 getAccelerometer(int accelerometerCode) {
		return null;
	}

	public void setAccelerometerSensitivity(float sensitivity) {
	}

	public String getName() {
		return null;
	}

	public void addListener(ControllerListener listener) {
	}

	public void removeListener(ControllerListener listener) {
	}

}
