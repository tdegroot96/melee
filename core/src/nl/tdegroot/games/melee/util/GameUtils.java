package nl.tdegroot.games.melee.util;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Filter;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import nl.tdegroot.games.melee.Game;
import nl.tdegroot.games.melee.level.Level;
import shaders.ShadowShader;

public class GameUtils {

    public static TextureRegion getCherryTexture(String texturePath, int tileWidth, int tileHeight, int row, int column) {
        Texture texture = new Texture(texturePath);
        TextureRegion[][] tmp = TextureRegion.split(texture, tileWidth, tileHeight);
        return tmp[row][column];
    }

    public static Animation getAnimation(String animationPath, int tileWidth, int tileHeight, int row, int columns, float frameDuration) {
        Texture texture = new Texture(animationPath);
        TextureRegion[][] tmp = TextureRegion.split(texture, tileWidth, tileHeight);
        TextureRegion[] walkFrames = new TextureRegion[columns];
        int index = 0;
        for (int i = 0; i < columns; i++) {
            walkFrames[index++] = tmp[row][i];
        }
        return new Animation(frameDuration, walkFrames);
    }

    public static PointLight createLight(RayHandler rayHandler, float x, float y, float radius, Color color) {
        Filter filter = new Filter();
        filter.categoryBits = Level.LIGHT_BIT;
        filter.maskBits = Level.COLLISION_BOX_BIT | Level.COLLISION_POLY_BIT;
        PointLight.setGlobalContactFilter(filter);
        return new PointLight(rayHandler, 1024, color, radius * 2 / Game.PPM, x / Game.PPM, y / Game.PPM);
    }

    public static FixtureDef createFixtureBox(float x, float y, float width, float height) {
        PolygonShape shape = new PolygonShape();
        FixtureDef fixtureDef = new FixtureDef();

        shape.setAsBox(width / Game.PPM, height / Game.PPM, new Vector2(x, -y / Game.PPM), 0);
        fixtureDef.shape = shape;
        return fixtureDef;
    }

    public static Color hexToColor(String hex) {
        hex = hex.replace("#", "");
        /** Get integer value from hex, shift it left by 8 bits and append full alpha */
        int hexValue = (Integer.parseInt(hex, 16) << 8);
        return new Color(hexValue);
    }

}
