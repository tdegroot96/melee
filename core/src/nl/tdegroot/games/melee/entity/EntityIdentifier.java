package nl.tdegroot.games.melee.entity;

import java.util.concurrent.atomic.AtomicLong;

public class EntityIdentifier {

	private static AtomicLong counter = new AtomicLong();

	public static void next() {
		counter.getAndIncrement();
	}

	public static long current() {
		return counter.get();
	}

}
