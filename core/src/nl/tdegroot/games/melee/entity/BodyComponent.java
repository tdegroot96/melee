package nl.tdegroot.games.melee.entity;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import nl.tdegroot.games.melee.Game;
import nl.tdegroot.games.melee.util.GameUtils;

public class BodyComponent {

    private World world;
    private String name;
    private Body body;

    public BodyComponent(World world, String name) {
        this.world = world;
        this.name = name;
    }

    public void setBox(float x, float y, float width, float height, float mass, int categoryBits, int maskBits) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set((x + width / 2) / Game.PPM, (y + height / 2) / Game.PPM);

        body = world.createBody(bodyDef);
        body.setFixedRotation(true);

        FixtureDef fixtureDef = GameUtils.createFixtureBox(0, 0, width / 2, height / 2);
        fixtureDef.filter.categoryBits = (short) categoryBits;
        fixtureDef.filter.maskBits = (short) maskBits;
        fixtureDef.density = mass;

        body.createFixture(fixtureDef).setUserData(name + " body");
    }

    public void addSensor(int x, int y, int width, int height, int categoryBits, int maskBits) {
        FixtureDef footDef = GameUtils.createFixtureBox(x, y, width / 2, 2);
        footDef.filter.categoryBits = (short) categoryBits;
        footDef.filter.maskBits = (short) maskBits;
        footDef.isSensor = true;
        body.createFixture(footDef).setUserData(name + " sensor");

        body.setLinearDamping(2);
    }

    public Body getBody() {
        return body;
    }
}
