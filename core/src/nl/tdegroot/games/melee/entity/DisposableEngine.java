package nl.tdegroot.games.melee.entity;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Disposable;

public class DisposableEngine extends Engine {

	public void dispose() {
		ImmutableArray<Entity> entities = getEntities();
		for (Entity e : entities) {
			ImmutableArray<Component> components = e.getComponents();
			for (Component component : components) {
				if (!(component instanceof Disposable)) continue;
				Disposable disposable = (Disposable) component;
				disposable.dispose();
				Gdx.app.log("Disposed", disposable.toString());
			}
		}
	}

}
