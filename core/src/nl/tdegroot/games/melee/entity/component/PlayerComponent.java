package nl.tdegroot.games.melee.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;
import nl.tdegroot.games.melee.util.GameUtils;

public class PlayerComponent implements Component, Disposable {

	public Animation walking;
	public Animation falling;
	public Animation idle;

	public PlayerComponent() {
		walking = GameUtils.getAnimation("niggerfaggot_walk_test.png", 64, 64, 0, 4, 0.125f);
		falling = GameUtils.getAnimation("niggerfaggot_falling.png", 64, 64, 0, 1, 5);
		idle = GameUtils.getAnimation("niggerfaggot_idle.png", 64, 64, 0, 2, 0.3f);
	}

	public void dispose() {
		for (TextureRegion region : idle.getKeyFrames()) {
			region.getTexture().dispose();
		}
		for (TextureRegion region : falling.getKeyFrames()) {
			region.getTexture().dispose();
		}
		for (TextureRegion region : walking.getKeyFrames()) {
			region.getTexture().dispose();
		}
	}

}
