package nl.tdegroot.games.melee.entity.component.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import nl.tdegroot.games.melee.entity.component.*;
import nl.tdegroot.games.melee.util.XboxMapping;

public class InputSystem extends EntitySystem {

	protected ImmutableArray<Entity> entities;
	private float animTime = 0;

	public InputSystem() {
		super(0);
	}

	public void addedToEngine (Engine engine) {
		entities = engine.getEntitiesFor(Family.all(PhysicsComponent.class, InputComponent.class, DrawingComponent.class, PlayerComponent.class, ContactComponent.class).get());
	}

	public void update(float deltaTime) {
		for (int i = 0; i < entities.size(); ++i) {
			Entity entity = entities.get(i);

			PhysicsComponent physicsComponent = Mapper.physics.get(entity);
			Body body = physicsComponent.body;

			InputComponent input = Mapper.input.get(entity);
			Controller controller = input.controller;

			DrawingComponent drawingComponent = Mapper.drawing.get(entity);
			PlayerComponent playerComponent = Mapper.player.get(entity);
			ContactComponent contactComponent = Mapper.jumping.get(entity);

			if (contactComponent.contacts > 0) {
				input.falling = false;
				input.climbing = false;
			} else {
				input.falling = true;
			}

			float horizontalMovement = 50f;
			float delta = Gdx.app.getGraphics().getDeltaTime();

			/**
			 * Different horizontal movement speed when climbing.
			 * Needs to be able to climb and needs to be climbing as well.
			 * Which means, the statement is false when jumping from an elevator.
			 */
			if (contactComponent.canClimb && input.climbing) {
				horizontalMovement = 10f;
			} else if (input.falling) {
				horizontalMovement = 20f;
			}

			horizontalMovement *= delta;

			if (Gdx.input.isKeyPressed(Input.Keys.A) || controller.getAxis(XboxMapping.L_STICK_HORIZONTAL_AXIS) < -0.25f) {
				body.applyLinearImpulse(new Vector2(-horizontalMovement, 0), body.getWorldCenter(), true);
				input.direction = 0;
				input.walking = true;
			}

			if (Gdx.input.isKeyPressed(Input.Keys.D) || controller.getAxis(XboxMapping.L_STICK_HORIZONTAL_AXIS) > 0.25f) {
				body.applyLinearImpulse(new Vector2(horizontalMovement, 0), body.getWorldCenter(), true);
				input.direction = 1;
				input.walking = true;
			}

			if ((Gdx.input.isKeyJustPressed(Input.Keys.W) || Gdx.input.isKeyJustPressed(Input.Keys.SPACE) || controller.getButton(XboxMapping.A)) && !input.falling && !contactComponent.canClimb) {
				body.applyLinearImpulse(new Vector2(0, 1400 * delta), body.getWorldCenter(), true);
			}

			if (contactComponent.canClimb) {
				if (Gdx.input.isKeyPressed(Input.Keys.W) || Gdx.input.isKeyPressed(Input.Keys.SPACE) || controller.getButton(XboxMapping.A)) {
					body.applyLinearImpulse(new Vector2(0, 90.0f * delta), body.getWorldCenter(), true);
					input.climbing = true;
				} else if (Math.abs(body.getLinearVelocity().y) > 0.0 && input.climbing) {
					body.applyLinearImpulse(new Vector2(0, 30f * delta), body.getWorldCenter(), true);
				} else {
					input.climbing = false;
				}
			}

			if (Math.abs(body.getLinearVelocity().x) < 0.4f) {
				input.walking = false;
			}

			if (contactComponent.contacts > 0) {
				input.falling = false;
				input.climbing = false;
			} else {
				input.falling = true;
			}

			animTime += Gdx.graphics.getDeltaTime();
			TextureRegion region;
			if (input.falling) {
				region = playerComponent.falling.getKeyFrame(animTime, true);
			} else if (input.walking) {
				region = playerComponent.walking.getKeyFrame(animTime, true);
			} else {
				region = playerComponent.idle.getKeyFrame(animTime, true);
			}

			Sprite currentSprite = drawingComponent.sprite;
			currentSprite.setRegion(region);

			if (input.direction == 0 && !currentSprite.isFlipX()) {
				currentSprite.flip(true, false);
			} else if (input.direction == 1) {
				currentSprite.flip(false, false);
			}

		}

	}
}
