package nl.tdegroot.games.melee.entity.component;

import com.badlogic.ashley.core.Component;
import nl.tdegroot.games.melee.entity.EntityIdentifier;
import nl.tdegroot.games.melee.level.ContactProvider;

public class ContactComponent implements Component {

	public int contacts = 0;
	public boolean canClimb;

	public ContactComponent() {
		/**
		 * To sensor please
		 */
		ContactProvider.register(EntityIdentifier.current(), this);
	}

}
