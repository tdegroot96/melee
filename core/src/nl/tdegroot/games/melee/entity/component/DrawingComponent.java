package nl.tdegroot.games.melee.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Disposable;

public class DrawingComponent implements Component, Disposable {

	public Sprite sprite = new Sprite();

	public void dispose() {
		sprite.getTexture().dispose();
	}

}
