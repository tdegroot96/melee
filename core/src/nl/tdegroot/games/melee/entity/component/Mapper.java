package nl.tdegroot.games.melee.entity.component;

import com.badlogic.ashley.core.ComponentMapper;

public class Mapper {

	public static final ComponentMapper<InputComponent> input = ComponentMapper.getFor(InputComponent.class);
	public static final ComponentMapper<PhysicsComponent> physics = ComponentMapper.getFor(PhysicsComponent.class);
	public static final ComponentMapper<DrawingComponent> drawing = ComponentMapper.getFor(DrawingComponent.class);
	public static final ComponentMapper<PlayerComponent> player = ComponentMapper.getFor(PlayerComponent.class);
	public static final ComponentMapper<ContactComponent> jumping = ComponentMapper.getFor(ContactComponent.class);

}
