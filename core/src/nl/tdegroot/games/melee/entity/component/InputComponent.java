package nl.tdegroot.games.melee.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import nl.tdegroot.games.melee.util.DummyController;

public class InputComponent implements Component {

	public int direction;

	public boolean walking;
	public boolean falling;
	public boolean climbing;

	public Controller controller;

	public InputComponent() {
		if (Controllers.getControllers().size > 0) {
			controller = Controllers.getControllers().first();
			Gdx.app.log("Controllers", "Using a controller called " + controller.getName());
		} else {
			Gdx.app.log("Controllers", "No controller connected");
			controller = new DummyController();
		}
	}

}
