package nl.tdegroot.games.melee.entity.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import nl.tdegroot.games.melee.Game;
import nl.tdegroot.games.melee.entity.EntityIdentifier;
import nl.tdegroot.games.melee.util.GameUtils;

public class PhysicsComponent implements Component, Disposable {

	public Body body;
	public Vector2 originalPosition;
	public Vector2 renderSize;
	public Vector2 boxSize;
	public World physicsWorld;
	public int categoryBits;
	public int maskBits;

	public PhysicsComponent(World physicsWorld) {
		this.physicsWorld = physicsWorld;
	}

	public void setBox(float x, float y, Vector2 boxSize, Vector2 renderSize, float mass, int categoryBits, int maskBits) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		bodyDef.fixedRotation = true;
		bodyDef.position.set((x + boxSize.x / 2) / Game.PPM, (y + boxSize.y / 2) / Game.PPM);

		originalPosition = new Vector2(x, y);

		this.renderSize = renderSize;
		this.boxSize = boxSize;
		this.categoryBits = categoryBits;
		this.maskBits = maskBits;

		body = physicsWorld.createBody(bodyDef);

		FixtureDef fixtureDef = GameUtils.createFixtureBox(0, 0, boxSize.x / 2, boxSize.y / 2);
		fixtureDef.filter.categoryBits = (short) categoryBits;
		fixtureDef.filter.maskBits = (short) maskBits;
		fixtureDef.density = mass;

		body.createFixture(fixtureDef).setUserData("body-box-" + EntityIdentifier.current());
	}

	public void addSensor() {
		FixtureDef fixtureDef = GameUtils.createFixtureBox(0, boxSize.y / 2, boxSize.x / 3, 2);
		fixtureDef.filter.categoryBits = (short) categoryBits;
		fixtureDef.filter.maskBits = (short) maskBits;
		fixtureDef.isSensor = true;
		body.setLinearDamping(2);

		body.createFixture(fixtureDef).setUserData("body-box-sensor-" + EntityIdentifier.current());
	}

	public void dispose() {
		physicsWorld.destroyBody(body);
	}

}
