package nl.tdegroot.games.melee.entity.component.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.utils.ImmutableArray;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import nl.tdegroot.games.melee.Game;
import nl.tdegroot.games.melee.entity.component.DrawingComponent;
import nl.tdegroot.games.melee.entity.component.Mapper;
import nl.tdegroot.games.melee.entity.component.PhysicsComponent;

public class RenderSystem extends EntitySystem {

	protected SpriteBatch batch;
	protected ImmutableArray<Entity> entities;

	public RenderSystem(SpriteBatch batch) {
		super(50);
		this.batch = batch;
	}

	public void addedToEngine(Engine engine) {
		entities = engine.getEntitiesFor(Family.all(PhysicsComponent.class, DrawingComponent.class).get());
	}

	public void update(float deltaTime) {
		batch.begin();

		for (int i = 0; i < entities.size(); i++) {
			Entity entity = entities.get(i);
			PhysicsComponent physicsComponent = Mapper.physics.get(entity);
			Body body = physicsComponent.body;
			Vector2 size = physicsComponent.renderSize;
			DrawingComponent drawingComponent = Mapper.drawing.get(entity);

			batch.draw(
					drawingComponent.sprite,
					body.getPosition().x - size.x / 2 / Game.PPM,
					(body.getPosition().y + 0.01f) - size.y / 2 / Game.PPM,
					drawingComponent.sprite.getRegionWidth() / Game.PPM,
					drawingComponent.sprite.getRegionHeight() / Game.PPM
			);
		}

		batch.end();
	}

}
